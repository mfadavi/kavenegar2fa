<?php
defined('WP_UNINSTALL_PLUGIN') or die();

if (function_exists('delete_metadata')) {
  delete_metadata('user', 0, 'kavenegar2fa_phone', '', true);
  delete_metadata('user', 0, 'kavenegar2fa_2fa_required', '', true);
}

if (function_exists('delete_option')) {
  delete_option('kavenegar2fa');
}
