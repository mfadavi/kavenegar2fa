jQuery && jQuery(function ($) {
  'use strict';

  var app = {};

  if (typeof kavenegar2fa !== 'object' || !kavenegar2fa) {
    return; // disable script
  }

  app.SEND_VERIF_URL = kavenegar2fa['send_verif_url'];
  app.SEND_VERIF_CAUSE = kavenegar2fa['cause'];
  app.DEFAULT_COUNTRY_CODE = kavenegar2fa['default_country_code'] || '98';
  app.PHONE_NUMBER_PATTERN = /^\s*\+?([0-9]{10,12})\s*$/;

  app.$phone = $('input#kavenegar2fa_phone');
  app.$sendCode = $('input#kavenegar2fa_send-code');
  app.$verif = $('input#kavenegar2fa_verif');
  app.$wpSubmit = $('input#wp-submit[type="submit"]');

  if (!app.$phone.length ||
      !app.$sendCode.length ||
      !app.$verif.length) {
    return; // disable script
  }

  app.main = function () {
    app.$sendCode.click(app.onSendCode);
  };

  app.sanitizedPhoneNumber = function () {
    var phone = app.$phone.val();
    if (!phone) { return null; }

    var m = phone.match(app.PHONE_NUMBER_PATTERN);
    if (!m || !m[1]) { return null; }

    phone = m[1];

    if (phone.startsWith('0')) {
      phone = phone.substr(1);
      phone = app.DEFAULT_COUNTRY_CODE + phone;
    }

    return phone;
  };

  app.disableUI = function () {
    app.$phone.attr('disabled', 'disabled');
    app.$sendCode.attr('disabled', 'disabled');
    app.$verif.attr('disabled', 'disabled');
    app.$wpSubmit.attr('disabled', 'disabled');
  };

  app.enableUI = function () {
    app.$phone.removeAttr('disabled');
    app.$sendCode.removeAttr('disabled');
    app.$verif.removeAttr('disabled');
    app.$wpSubmit.removeAttr('disabled');
  };

  app.onSendCode = function () {
    app.$verif.val('');

    var phone = app.sanitizedPhoneNumber();

    if (!phone) {
      app.$phone.blur();
      app.$phone.select();
      return;
    }

    app.$phone.val(phone);
    app.disableUI();

    $.ajax({
      method: 'POST',
      url: app.SEND_VERIF_URL,
      data: {
        phone: phone,
        cause: app.SEND_VERIF_CAUSE
      },
    }).then(
      function (data) {
        app.enableUI();
      },
      function (xhr, status, err) {
        app.enableUI();
        /* (fadavi.net) TODO: somehow notify the user */
      }
    );
  };

  app.main();

});
