<?php

defined('ABSPATH') or die();

class Kavenegar2FA_Admin
{
  private $root;
  private $utils;
  private $basename;

  public function __construct ($scope)
  {
    $this->root = $scope->root;
    $this->utils = $scope->utils;
    $this->basename = $scope->basename;
  }

  public function initialize ()
  {
    add_action('admin_init', [$this, 'on_admin_init']);
    add_action('admin_menu', [$this, 'on_admin_menu']);
    add_action('admin_notices', [$this, 'on_admin_notices']);

    add_filter("plugin_action_links_{$this->basename}",
               [$this, 'filter_links'], 10, 1);
  }

  public function on_admin_notices ()
  {
    global $pagenow;

    if ($pagenow == 'options-general.php') {
      return;
    }

    $has_api_key = (bool) $this->utils->get_option('api_key');
    $has_verif_template = (bool) $this->utils->get_option('verif_template');

    if ($has_api_key && $has_verif_template) {
      return;
    }

    /* translators: %s: options page URL */
    $content = __('API key and/or verification template is not configured and no SMS will sent. Click <a href="%s">here</a> to configure.', 'kavenegar2fa');

    $subroute = 'options-general.php?page=kavenegar2fa_options';
    $content = sprintf($content, admin_url($subroute));

    $this->utils->admin_notice([
      'content' => $content
    ]);
  }

  public function on_admin_init ()
  {
    register_setting('kavenegar2fa_group', 'kavenegar2fa');

    add_settings_section(
      'kavenegar2fa_options_section',
      __('General', 'kavenegar2fa'),
      [$this, 'render_options_section'],
      'kavenegar2fa_group'
    );

    add_settings_field(
      'kavenegar2fa_api-key',
      __('Kavenegar 2FA API Key', 'kavenegar2fa'),
      [$this, 'render_api_key_field'],
      'kavenegar2fa_group',
      'kavenegar2fa_options_section',
      [
        'label_for' => 'kavenegar2fa_api-key',
        'option_name' => 'kavenegar2fa',
        'name' => 'api_key',
        'value' => $this->utils->get_option('api_key', '')
      ]
    );

    add_settings_field(
      'kavenegar2fa_verif-template',
      __('Verification template', 'kavenegar2fa'),
      [$this, 'render_verif_template_field'],
      'kavenegar2fa_group',
      'kavenegar2fa_options_section',
      [
        'label_for' => 'kavenegar2fa_verif-template',
        'option_name' => 'kavenegar2fa',
        'name' => 'verif_template',
        'value' => $this->utils->get_option('verif_template', '')
      ]
    );

    add_settings_field(
      'kavenegar2fa_phone-required',
      __('Phone is required?', 'kavenegar2fa'),
      [$this, 'render_phone_required_field'],
      'kavenegar2fa_group',
      'kavenegar2fa_options_section',
      [
        'label_for' => 'kavenegar2fa_phone-required',
        'option_name' => 'kavenegar2fa',
        'name' => 'phone_required',
        'value' => $this->utils->get_option('phone_required', false)
      ]
    );
  }

  public function render_verif_template_field ($args)
  {
    $this->utils->render('admin-input-text', [
      'id' => $args['label_for'],
      'name' => sprintf('%s[%s]', $args['option_name'], $args['name']),
      'value' => $args['value'],
      'code' => true,
    ]);
  }

  public function render_phone_required_field ($args)
  {
    $this->utils->render('admin-input-checkbox', [
      'id' => $args['label_for'],
      'name' => sprintf('%s[%s]', $args['option_name'], $args['name']),
      'checked' => $args['value'],
    ]);
  }

  public function render_api_key_field ($args)
  {
    $this->utils->render('admin-input-text', [
      'id' => $args['label_for'],
      'name' => sprintf('%s[%s]', $args['option_name'], $args['name']),
      'value' => $args['value'],
      'code' => true,
    ]);
  }

  public function render_options_section ()
  {
  }

  public function on_admin_menu ()
  {
    add_options_page(
      __('General', 'kavenegar2fa'),
      __('Kavenegar 2FA', 'kavenegar2fa'),
      'manage_options',
      'kavenegar2fa_options',
      [$this, 'render_options_page']
    );
  }

  public function render_options_page ()
  {
    $this->utils->render('options-page');
  }

  public function filter_links ($links)
  {
    $settings_url = admin_url('options-general.php?page=kavenegar2fa_options');
    $link = "<a href=\"$settings_url\">";
    $link .= esc_html__('Settings', 'kavenegar2fa');
    $link .= '</a>';

    $links[] = $link;
    return $links;
  }

}
