<?php

defined('ABSPATH') or die();

class Kavenegar2FA_Session
{
  private const KEY = 'kavenegar2fa';

  public function __construct ()
  {
    if (session_status() === PHP_SESSION_DISABLED) {
      $msg = __('Kavenegar2fa needs PHP sessions to work', 'kavenegar2fa');
      $this->fatal($msg);
    }
  }

  private function fatal ($m)
  {
    if (function_exists('wp_die')) {
      wp_die($m);
    } else {
      die($m);
    }
  }

  private function ensure_session ()
  {
    if (session_status() !== PHP_SESSION_ACTIVE) {
      session_set_cookie_params(['path' => site_url()]);
      session_start();
    }

    if (session_status() !== PHP_SESSION_ACTIVE || !isset($_SESSION)) {
      $msg = __('Kavenegar2FA: PHP Session not loaded', 'kavenegar2fa');
      $this->fatal($msg);
    }
  }

  private function is_empty ()
  {
    return empty($_SESSION[self::KEY]);
  }

  private function current_user_id ()
  {
    if (function_exists('get_current_user_id')) {
      return get_current_user_id();
    }
    return 0;
  }

  private function session_user_id ()
  {
    return (int) $this->internal_get('user_id');
  }

  private function ensure_identity ()
  {
    $this->ensure_session();

    if ($this->is_empty()) {
      return true;
    }

    $current_user_id = $this->current_user_id();
    $session_user_id = $this->session_user_id();

    if ($session_user_id === $current_user_id) {
      return true;
    }

    $this->internal_clear();
    return false;
  }

  private function assign_user_id ()
  {
    if (isset($_SESSION[self::KEY]['user_id'])) {
      return;
    }

    $uid = $this->current_user_id();
    $uid and $_SESSION[self::KEY]['user_id'] = $uid;
  }

  private function internal_get ($key)
  {
    if (isset($_SESSION[self::KEY]) &&
        is_array($_SESSION[self::KEY]) &&
        isset($_SESSION[self::KEY][$key])) {
      return $_SESSION[self::KEY][$key];
    }

    return null;
  }

  private function internal_set ($key, $value)
  {
    if (!isset($_SESSION[self::KEY])) {
      $_SESSION[self::KEY] = [];
    }

    $_SESSION[self::KEY][$key] = $value;
  }

  private function internal_clear ()
  {
    unset($_SESSION[self::KEY]);
  }

  public function is_cleared ()
  {
    if (empty($_SESSION[self::KEY])) {
      return true;
    }

    if (count($_SESSION[self::KEY]) === 1 &&
        isset($_SESSION[self::KEY]['user_id'])) {
      return true;
    }

    return false;
  }

  public function exists ()
  {
    return isset($_SESSION[self::KEY]);
  }

  public function get ($key)
  {
    if (!$this->ensure_identity()) {
      return null;
    }

    return $this->internal_get($key);
  }

  public function set ($key, $value)
  {
    $this->ensure_identity();
    $this->internal_set($key, $value);
    $this->assign_user_id();
  }

  public function is ($key, $value)
  {
    return $this->get($key) == $value;
  }

  public function unset (...$keys)
  {
    $this->ensure_identity();

    foreach ($keys as $key) {
      unset($_SESSION[self::KEY][$key]);
    }
  }

  public function has ($key)
  {
    return $this->get($key) !== null;
  }

  public function clear ()
  {
    $this->ensure_identity();
    $this->internal_clear();
  }

  public function get_all ()
  {
    $this->ensure_identity();

    if (isset($_SESSION[self::KEY])) {
      return $_SESSION[self::KEY];
    }

    return [];
  }

  public function set_all ($all)
  {
    $this->ensure_identity();

    $_SESSION[self::KEY] = $all;
    $this->is_empty() or $this->assign_user_id();
  }
}
