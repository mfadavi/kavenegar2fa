<?php

defined('ABSPATH') or die();

class Kavenegar2FA_REST
{
  private $namespace;

  private $utils;
  private $api;
  private $session;

  public function __construct ($scope)
  {
    $this->utils = $scope->utils;
    $this->api = $scope->api;
    $this->session = $scope->session;

    $this->namespace = 'kavenegar2fa/v1';
  }

  public function initialize ()
  {
    add_action('rest_api_init', [$this, 'on_rest_api_init']);
  }

  public function on_rest_api_init ()
  {
    register_rest_route($this->namespace, '/send-verif', [
      'methods' => 'POST',
      'callback' => [$this, 'rest_send_verif'],
      'args' => [
        'phone' => [
          'sanitize_callback' => [$this->utils, 'sanitize_phone_number']
        ],
        'cause' => [
          'validate_callback' => [$this->utils, 'validate_cause']
        ]
      ]
    ]);
  }

  public function rest_send_verif ($req)
  {
    $phone = $req['phone'];
    $cause = $req['cause'];

    if (!$cause) {
      return [
        'result' => 'nok',
        'message' => __('Error: cause is required', 'kavenegar2fa')
      ];
    }
    if (!$phone) {
      return [
        'result' => 'nok',
        'message' => __('Error: invalid phone number', 'kavenegar2fa')
      ];
    }

    /* (fadavi.net) TODO: check request rate... */

    $verif = $this->utils->generate_verif_code();

    $this->session->set_all([
      'state' => 'register_retrieve_verif',
      "${cause}_verif" => $verif,
      "${cause}_phone" => $phone
    ]);

    $sms_error = $this->api->send_verif_code($phone, $verif);

    if (is_wp_error($sms_error)) {
      $msg = __('Error: SMS not sent. Please, try again.', 'kavenegar2fa');
      return ['result' => 'nok', 'message' => $msg];
    }

    return ['result' => 'ok'];
  }
}
