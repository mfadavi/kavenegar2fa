<?php

defined('ABSPATH') or die();

function kavenegar2fa_setup ($scope)
{
  function_exists('kavenegar2fa_includes') and die('Dup. fn: kavenegar2fa_includes');
  require_once $scope->root . 'inc/function.kavenegar2fa-includes.php';

  kavenegar2fa_includes($scope->root);

  $scope->utils = new Kavenegar2FA_Utils($scope);
  $scope->session = new Kavenegar2FA_Session($scope);
  $scope->api = new Kavenegar2FA_API($scope);
  $scope->admin = new Kavenegar2FA_Admin($scope);
  $scope->login = new Kavenegar2FA_Login($scope);
  $scope->profile = new Kavenegar2FA_Profile($scope);
  $scope->rest = new Kavenegar2FA_REST($scope);

  $plugin = new Kavenegar2FA_Plugin($scope);
  $plugin->initialize();
}
