<?php

defined('ABSPATH') or die();

function kavenegar2fa_includes ($root)
{
  $includes = [
    'Kavenegar2FA_Plugin' => 'inc/class.kavenegar2fa-plugin.php',
    'Kavenegar2FA_Admin' => 'inc/class.kavenegar2fa-admin.php',
    'Kavenegar2FA_Utils' => 'inc/class.kavenegar2fa-utils.php',
    'Kavenegar2FA_API' => 'inc/class.kavenegar2fa-api.php',
    'Kavenegar2FA_Login' => 'inc/class.kavenegar2fa-login.php',
    'Kavenegar2FA_REST' => 'inc/class.kavenegar2fa-rest.php',
    'Kavenegar2FA_Session' => 'inc/class.kavenegar2fa-session.php',
    'Kavenegar2FA_Profile' => 'inc/class.kavenegar2fa-profile.php',
  ];

  foreach ($includes as $class_name => $class_path) {
    class_exists($class_name) and die("Dup. class: $class_name");
    require_once "{$root}{$class_path}";
  }
}
