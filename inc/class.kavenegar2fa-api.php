<?php

defined('ABSPATH') or die();

require_once ABSPATH . 'wp-includes/class-http.php';

class Kavenegar2FA_API
{
  private const BASE_URL = 'https://api.kavenegar.com/v1';

  private $utils;

  public function __construct ($scope)
  {
    $this->utils = $scope->utils;
  }

  private function api_key ()
  {
    return $this->utils->get_option('api_key', false);
  }

  private function verif_template ()
  {
    return $this->utils->get_option('verif_template', false);
  }

  private function url ($path)
  {
    $api_key = $this->api_key();
    if (!$api_key) { return false; }

    return self::BASE_URL . "/$api_key/$path.json";
  }

  public function verify_lookup ($receptor, $template, $tokens)
  {
    $url = $this->url('verify/lookup');
    if (!$url) { return false; }

    is_array($tokens) or $tokens = [$tokens];

    $body = [
      'receptor' => $receptor,
      'template' => $template,
      'token' => $tokens[0]
    ];
    isset($tokens[1]) and $body['token2'] = $tokens[1];
    isset($tokens[2]) and $body['token3'] = $tokens[2];

    $http = new WP_Http();
    return $http->request($url, [
      'method' => 'POST',
      'body' => $body
    ]);
  }

  public function send_verif_code ($receptor, $verif_code)
  {
    $verif_template = $this->verif_template();
    if (!$verif_template) { return false; }

    return $this->verify_lookup($receptor, $verif_template, $verif_code);
  }
}
