<?php

defined('ABSPATH') or die();

class Kavenegar2FA_Login
{
  private $LOGIN_ACTIONS = [
    'kavenegar2fa_login_retrieve_phone',
    'kavenegar2fa_login_retrieve_verif',
    'kavenegar2fa_register_retrieve_phone',
    'kavenegar2fa_register_retrieve_verif',
  ];

  private $main;
  private $utils;
  private $api;
  private $session;

  public function __construct ($scope)
  {
    $this->main = $scope->main;
    $this->utils = $scope->utils;
    $this->api = $scope->api;
    $this->session = $scope->session;
  }

  public function initialize ()
  {
    add_action('register_form', [$this, 'on_register_form']);
    add_action('login_enqueue_scripts', [$this, 'on_login_enqueue_scripts']);
    add_action('register_post', [$this, 'on_register_post'], 10, 3);
    add_action('user_register', [$this, 'on_user_register'], 10, 1);

    add_action('login_form_kavenegar2fa_login_retrieve_phone',
               [$this, 'on_login_retrieve_phone']);
    add_action('login_form_kavenegar2fa_login_retrieve_verif',
               [$this, 'on_login_retrieve_verif']);

    add_filter('login_redirect', [$this, 'filter_login_redirect'], 20, 3);

    add_action('init', [$this, 'on_init']);
  }

  public function on_init ()
  {
    global $pagenow;

    if ($pagenow === 'wp-login.php') {
      return;
    }

    $logged_in = (bool) is_user_logged_in();
    $authd = (bool) $this->session->is('state', 'authenticated');

    if ($logged_in && $authd) {
      return;
    }

    if (!$logged_in && !$authd) {
      $this->session->unset('state', 'login_phone', 'login_verif');
      return;
    }

    $this->session->clear();
    wp_logout();
  }

  public function filter_login_redirect (
    $redirect_to,
    $requested_redirect_to,
    $user
  ) {
    return add_query_arg(
      [
        'action' => 'kavenegar2fa_login_retrieve_phone',
        'redirect_to' => $redirect_to
      ],
      wp_login_url()
    );
  }

  private function unnecessary_2fa ()
  {
    $to = empty($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : null;

    if (is_user_logged_in()) {
      $this->session->set_all(['state' => 'authenticated']);
      $to or $to = admin_url();
    } else {
      $this->session->unset('state', 'login_phone', 'login_verif');
      $to or $to = wp_login_url();
    }

    $this->utils->login_delayed_redirect([
      'to' => $to,
      'delay' => 2
    ]);
  }

  private function access_denied ()
  {
    $msg = __('You are not authorized to access this page', 'kavenegar2fa');
    $errors = new WP_Error('access_denined', $msg);

    $this->utils->login_delayed_redirect(['errors' => $errors]);
  }

  private function invalid_state ()
  {
    $this->session->clear();
    wp_logout();

    $msg = __('<strong>Error</strong>: Authentication terminated', 'kavenegar2fa');
    $errors = new WP_Error('auth_progress_terminated', $msg);

    $this->utils->login_delayed_redirect(['errors' => $errors]);
  }

  private function phone_changed ()
  {
    $this->session->unset('state', 'login_phone', 'login_verif');
    wp_logout();

    $msg = __('Your phone number has been changed during the progress', 'kavenegar2fa');
    $errors = new WP_Error('phone_changed', $msg);

    $this->utils->login_delayed_redirect([
      'to' => wp_login_url(),
      'delay' => 5,
      'errors' => $errors
    ]);
  }

  private function login_retrieve_phone_handle_post ()
  {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
      return null;
    }

    if (!isset($_POST['kavenegar2fa_phone'])) {
      $msg = __('<strong>Error</strong>: Enter your phone number', 'kavenegar2fa');
      return new WP_Error('empty_phone', $msg);
    }

    $phone = $_POST['kavenegar2fa_phone'];
    $phone = $this->utils->sanitize_phone_number($phone);

    if (!$phone) {
      $msg = __('<strong>Error</strong>: Invalid phone number', 'kavenegar2fa');
      return new WP_Error('invalid_phone', $msg);
    }

    $uid = get_current_user_id();
    $correct_phone = get_user_meta($uid, 'kavenegar2fa_phone', true);
    $correct_phone or wp_die(__('There is no phone meta value', 'kavenegar2fa'));

    if ($correct_phone !== $phone) {
      $msg = __('<strong>Error</strong>: Wrong phone number', 'kavenegar2fa');
      return new WP_Error('wrong_phone', $msg);
    }

    $verif = $this->utils->generate_verif_code();
    $error = $this->api->send_verif_code($correct_phone, $verif_code);

    if (is_wp_error($error)) {
      $msg = __('<strong>Error</strong>: SMS not sent. Try again, later.', 'kavenegar2fa');
      return new WP_Error('sending_failed', $msg);
    }

    $this->session->set_all([
      'state' => 'login_retrieve_verif',
      'login_verif' => $verif,
      'login_phone' => $correct_phone
    ]);

    return true;
  }

  public function on_login_retrieve_phone ()
  {
    $has_api_key = (bool) $this->utils->get_option('api_key');
    $has_verif_template = (bool) $this->utils->get_option('verif_template');
    $current_user = wp_get_current_user();
    if (!$has_api_key ||
        !$has_verif_template ||
        !$this->utils->is_2fa_required($current_user)) {
      $this->unnecessary_2fa();
      exit();
    }

    if (!$this->session->is_cleared()) {
      $this->invalid_state();
      exit();
    }

    $errors = $this->login_retrieve_phone_handle_post();
    if ($errors === true) {
      $action = 'kavenegar2fa_login_retrieve_verif';
      $url = add_query_arg('action', $action, wp_login_url());
      wp_redirect($url);
      exit();
    }

    $user_id = get_current_user_id();
    $phone = get_user_meta($user_id, 'kavenegar2fa_phone', true);
    $hint = $this->utils->phone_hint($phone);

    login_header(
      __('Phone Number', 'kavenegar2fa'),
      __('Enter your phone number', 'kavenegar2fa'),
      $errors
    );

    $this->utils->render('login-retrieve-phone', [
      'hint' => $hint,
    ]);

    login_footer();
    exit();
  }

  private function login_retrieve_verif_handle_post ()
  {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
      return null;
    }

    if (!isset($_POST['kavenegar2fa_verif'])) {
      $msg = __('<strong>Error</strong>: Enter verification code', 'kavenegar2fa');
      return new WP_Error('empty_verif', $msg);
    }

    $verif = $_POST['kavenegar2fa_verif'];
    $verif = $this->utils->sanitize_verif_code($verif);

    if (!$verif) {
      $msg = __('<strong>Error</strong>: Invalid verificatin code', 'kavenegar2fa');
      return new WP_Error('invalid_verif', $msg);
    }

    $correct_verif = $this->session->get('login_verif');

    if ($verif !== $correct_verif) {
      $msg = __('<strong>Error</strong>: Wrong verification code', 'kavenegar2fa');
      return new WP_Error('wrong_verif', $msg);
    }

    $this->session->set_all([
      'state' => 'authenticated'
    ]);

    return true;
  }

  public function on_login_retrieve_verif ()
  {
    $current_user = wp_get_current_user();
    if (!$this->utils->is_2fa_required($current_user)) {
      $this->access_denied();
      exit();
    }

    if (!$this->session->is('state', 'login_retrieve_verif') ||
        !$this->session->has('login_verif') ||
        !$this->session->has('login_phone')) {
      $this->invalid_state();
      exit();
    }

    $user_id = get_current_user_id();
    $user_phone = get_user_meta($user_id, 'kavenegar2fa_phone', true);
    $session_phone = $this->session->get('login_phone');

    if ($session_phone !== $user_phone) {
      $this->phone_changed();
      exit();
    }

    $errors = $this->login_retrieve_verif_handle_post();
    if ($errors === true) {
      $has_redirect_to = isset($_REQUEST['redirect_to']);
      $url = $has_redirect_to ? $_REQUEST['redirect_to'] : admin_url();
      wp_safe_redirect($url);
      exit();
    }

    login_header(
      __('Verification Code', 'kavenegar2fa'),
      __('Enter verification code sent to you via SMS', 'kavenegar2fa'),
      $errors
    );

    $this->utils->render('login-retrieve-verif');

    login_footer();
    exit();
  }

  public function on_register_form ()
  {
    $view = [];

    if (!empty($_REQUEST['kavenegar2fa_phone'])) {
      $view['phone_value'] = $_REQUEST['kavenegar2fa_phone'];
    }

    $this->utils->render('phone-and-verif', $view);
  }

  public function on_login_enqueue_scripts ()
  {
    $action = $this->utils->login_action();
    if ($action !== 'register') {
      return;
    }

    wp_register_script(
      'kavenegar2fa_verif-phone',
      plugins_url('assets/verif-phone.js', $this->main),
      ['jquery']
    );

    $send_verif_url = site_url('/?rest_route=/kavenegar2fa/v1/send-verif');
    $cause = $action;
    $default_country_code = $this->utils->default_country_code();
    wp_localize_script(
      'kavenegar2fa_verif-phone',
      'kavenegar2fa',
      [
        'send_verif_url' => $send_verif_url,
        'cause' => $cause,
        'default_country_code' => $default_country_code,
      ]
    );

    wp_enqueue_script('kavenegar2fa_verif-phone');
  }

  public function on_register_post ($username, $email, WP_Error $errors)
  {
    $session = $this->session;

    if (is_wp_error($errors) && $errors->has_errors()) {
      $session->clear();
      return;
    }

    $phone = null;
    $verif = null;

    $phone_required = (bool) $this->utils->get_option('phone_required', false);
    $empty_phone = empty($_POST['kavenegar2fa_phone']);
    $empty_verif = empty($_POST['kavenegar2fa_verif']);

    if ($phone_required && $empty_phone) {
      $msg = __('<strong>Error</strong>: Enter your phone number', 'kavenegar2fa');
      $errors = $this->utils->error($errors, 'empty_phone', $msg);
    } elseif (!$empty_phone) {
      $phone = $_POST['kavenegar2fa_phone'];
      $phone = $this->utils->sanitize_phone_number($phone);

      if (!$phone) {
        $msg = __('<strong>Error</strong>: Invalid phone number', 'kavenegar2fa');
        $errors = $this->utils->error($errors, 'invalid_phone', $msg);
      }
    }

    if ($phone_required && $empty_verif) {
      $msg = __('<strong>Error</strong>: Enter received verification code', 'kavenegar2fa');
      $errors = $this->utils->error($errors, 'empty_verif', $msg);
    } elseif (!$empty_verif) {
      $verif = $_POST['kavenegar2fa_verif'];
      $verif = $this->utils->sanitize_verif_code($verif);

      if (!$verif) {
        $msg = __('<strong>Error</strong>: Invalid verification code', 'kavenegar2fa');
        $errors = $this->utils->error($errors, 'invalid_verif', $msg);
      }
    }

    if ($errors->has_errors()) {
      $session->clear();
      return;
    }

    if (!$phone && !$verif) {
      $session->set_all([
        'state' => 'register_create_user',
        'register_phone' => null
      ]);
      return;
    }

    if (!$session->is('register_phone', $phone)) {
      $session->clear();
      $msg = __('<strong>Error</strong>: Wrong phone number', 'kavenegar2fa');
      $this->utils->error($errors, 'wrong_phone', $msg);
      return;
    }

    if (!$session->is('register_verif', $verif)) {
      $session->clear();
      $msg = __('<strong>Error</strong>: Wrong verification code', 'kavenegar2fa');
      $this->utils->error($errors, 'wrong_verif', $msg);
      return;
    }

    $session->set_all([
      'state' => 'register_create_user',
      'register_phone' => $session->get('register_phone')
    ]);
  }

  public function on_user_register ($user_id)
  {
    if (!$this->session->is('state', 'register_create_user') || !$user_id) {
      return;
    }

    $phone = $this->session->get('register_phone');

    if (!$phone) {
      return;
    }

    $result = update_user_meta($user_id, 'kavenegar2fa_phone', $phone);

    if (!$result) {
      wp_delete_user($user_id);
    }
  }
}
