<?php

defined('ABSPATH') or die();

class Kavenegar2FA_Utils
{
  private const PHONE_NUMBER_PATTERN = '/^\s*\+?([0-9]{10,12})\s*$/';
  private const VERIF_CODE_PATTERN = '/^\s*([0-9]{5})\s*$/';

  private $opts = null;
  private $root;
  private $debug;

  public function __construct ($scope)
  {
    $this->root = $scope->root;
    $this->debug = $scope->debug;
  }

  public function default_country_code ()
  {
    return '98';
  }

  public function render ($view_name, $view = [])
  {
    is_array($view) and $view = (object) $view;
    include $this->root . "views/$view_name.phtml";
  }

  public function get_options ($force_reload = false)
  {
    if (!$this->opts || $force_reload) {
      $this->opts = get_option('kavenegar2fa', []);
    }

    return $this->opts;
  }

  public function get_option ($name, $default = false)
  {
    $opts = $this->get_options();
    return isset($opts[$name]) ? $opts[$name] : $default;
  }

  public function set_option ($name, $value)
  {
    $opts = $this->get_options();
    $opts[$name] = $value;
    set_option('kavenegar2fa', $opts);
  }

  public function generate_verif_code ()
  {
    if ($this->debug) {
      return '11111';
    }

    return (string) wp_rand(10000, 99999);
  }

  public function validate_cause($cause)
  {
    $valid_causes = ['login', 'register', 'profile'];
    return in_array($cause, $valid_causes);
  }

  public function sanitize_phone_number ($phone)
  {
    if (!$phone) { return false; }
    is_string($phone) or $phone = (string) $phone;

    $m = [];

    $matched = preg_match(self::PHONE_NUMBER_PATTERN, $phone, $m);
    if (!$matched || empty($m[1])) { return false; }

    $phone = $m[1];

    if (substr($phone, 0, 1) === '0') {
      $phone = substr($phone, 1, strlen($phone) - 1);
      $phone = $this->default_country_code() . $phone;
    }

    return $phone;
  }

  public function format_phone_number ($phone)
  {
    if (!$phone || !is_scalar($phone)) {
      return '';
    }

    if (!substr($phone, 0, 1) !== '+') {
      $phone = "+$phone";
    }

    return $phone;
  }

  public function sanitize_verif_code ($verif)
  {
    if (!$verif) { return false; }
    is_string($verif) or $phone = (string) $phone;

    $m = [];

    $matched = preg_match(self::VERIF_CODE_PATTERN, $verif, $m);
    if (!$matched || empty($m[1])) { return false; }

    $verif = $m[1];

    return $verif;
  }

  public function login_action ()
  {
    if (isset($_GET['key'])) { return 'resetpass'; }
    if (!isset($_REQUEST['action'])) { return 'login'; }

    $action = $_REQUEST['action'];
    $default_actions = [
      'confirm_admin_email',
      'postpass',
      'logout',
      'lostpassword',
      'retrievepassword',
      'resetpass',
      'rp',
      'register',
      'login',
      'confirmaction',
      WP_Recovery_Mode_Link_Service::LOGIN_ACTION_ENTERED,
    ];

    if (in_array($action, $default_actions, true) ||
        has_filter( 'login_form_' . $action)) {
      return $action;
    }

    return 'login';
  }

  public function is_2fa_required ($user = null)
  {
    if (is_wp_error($user) || !isset($user->ID) || $user->ID == 0) {
      return false;
    }

    $phone = get_user_meta(
      $user->ID,
      'kavenegar2fa_phone',
      true
    );

    if (!$phone) {
      return false;
    }

    $twofa_required = get_user_meta(
      $user->ID,
      'kavenegar2fa_2fa_required',
      true
    );

    return (bool) $twofa_required;
  }

  public function phone_hint ($phone)
  {
    $len = strlen($phone);
    $start = substr($phone, 0, 4);
    $end = substr($phone, $len - 1, $len);

    return "+$start*******$end";
  }

  public function login_delayed_redirect ($opts)
  {
    $opts = (object) array_merge([
      'to' => wp_login_url(),
      'delay' => 5,
      'message' => null,
      'errors' => null
    ], $opts);

    if (!isset($opts->title)) {
      $opts->title = __('Redirect', 'kavenegar2fa');
    }

    headers_sent() or header("refresh={$opts->delay};url={$opts->to}");

    login_header(
      $opts->title,
      $opts->message,
      $opts->errors
    );

    $this->render('login-delayed-redirect', $opts);
    login_footer();
  }

  public function admin_notice ($opts)
  {
    $opts = (object) array_merge([
      'type' => 'warning',
      'dismissible' => false
    ], $opts);

    $this->render('admin-notice', $opts);
  }

  public function find_user_id_by_phone ($phone)
  {
    if (!$phone) {
      return null;
    }

    $user_ids = get_users([
      'fields' => 'ID',
      'meta_key' => 'kavenegar2fa_phone',
      'meta_value' => $phone,
      'meta_compare' => '=',
    ]);

    $cnt = is_countable($user_ids) ? count($user_ids) : 0;

    if ($cnt === 0) {
      return null;
    }

    if ($cnt === 1) {
      $uid = $user_ids[0];
      return $uid ? $uid : null;
    }

    wp_die(__('Found users w/ same phone number', 'kavenegar2fa'));
  }

  public function parse_request_str ($name)
  {
    $str = (object) [];
    $str->empty = empty($_REQUEST[$name]);

    if ($str->empty) {
      $str->value = null;
    } else {
      $str->value = (string) $_REQUEST[$name];
    }

    return $str;
  }

  public function sanitize_request_bool ($name)
  {
    $bool = $this->parse_request_str($name);
    $bool->value = (bool) $bool->value;
    return $bool;
  }

  public function sanitize_request_phone ()
  {
    $phone = $this->parse_request_str('kavenegar2fa_phone');
    if (!$phone->empty) {
      $phone->value = $this->sanitize_phone_number($phone->value);
    }

    return $phone;
  }

  public function sanitize_request_verif ()
  {
    $verif = $this->parse_request_str('kavenegar2fa_verif');

    if (!$verif->empty) {
      $verif->value = $this->sanitize_verif_code($verif->value);
    }

    return $verif;
  }

  public function error (
    $errors = null,
    $code = '',
    $message = '',
    $data = ''
  ) {
    if (!is_wp_error($errors)) {
      return new WP_Error($code, $message, $data);
    }

    $errors->add($code, $message, $data);
    return $errors;
  }
}
