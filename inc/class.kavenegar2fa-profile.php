<?php

defined('ABSPATH') or die();

class Kavenegar2FA_Profile
{
  private $session;
  private $utils;
  private $api;

  public function __construct ($scope)
  {
    $this->session = $scope->session;
    $this->utils = $scope->utils;
    $this->api = $scope->api;
  }

  public function initialize ()
  {
    add_action('admin_notices', [$this, 'on_notices']);

    add_action('show_user_profile',
               [$this, 'on_show_own_profile'], 10, 1);
    add_action('edit_user_profile',
               [$this, 'on_show_user_profile'], 10, 1);

    add_filter('user_profile_update_errors',
               [$this, 'filter_update_errors'], 10, 3);

    add_action('personal_options_update',
               [$this, 'on_update_own_profile'], 10, 1);
    add_action('edit_user_profile_update',
               [$this, 'on_update_user_profile'], 10, 1);

    add_action('user_new_form',
               [$this, 'on_user_new_form'], 10, 1);
    add_action('user_register',
               [$this, 'on_user_register'], 10, 1);
  }

  private function render_phone_change_request_notice ()
  {
    $new_phone = $this->session->get('new_phone');
    $new_phone = $this->utils->format_phone_number($new_phone);

    $url = admin_url('profile.php#kavenegar2fa_2fa-required-wrap');
    $url = esc_url($url);

    /* translators: %s: phone number */
    $line1 = __('There is a pending request to change your phone number to <code>%s</code>.', 'kavenegar2fa');
    $line1 = sprintf($line1, $new_phone);

    /* translators: %s: cancelation URL */
    $line2 = __('To verify your new phone number or cancel the request, click <a href="%s">here</a>', 'kavenegar2fa');
    $line2 = sprintf($line2, $url);

    $html = implode('<br>', [$line1, $line2]);

    $this->utils->admin_notice([
      'type' => 'warning',
      'dismissible' => false,
      'content' => $html
    ]);
  }

  public function on_notices ()
  {
    global $pagenow;

    if ($pagenow === 'profile.php') {
      return;
    }

    $new_phone = $this->session->get('new_phone');
    $has_verif = $this->session->has('profile_verif');

    // if session has not phone and/or verif, there's nothing to do
    if ($new_phone && $has_verif) {
      $this->render_phone_change_request_notice();
    }
  }

  private function new_phone_currently_exists ()
  {
    $new_phone = $this->session->get('new_phone');

    if (!$new_phone) {
      return false;
    }

    $user_id = $this->utils->find_user_id_by_phone($new_phone);

    return (bool) $user_id;
  }

  private function forget_new_phone_if_currently_exists ()
  {
    if ($this->new_phone_currently_exists()) {
      $this->session->unset('new_phone');
      $this->session->unset('profile_verif');
      return true;
    }

    return false;
  }

  private function cancel_request_if_needed ()
  {
    if ($_SERVER['REQUEST_METHOD'] !== 'GET' ||
        !isset($_GET['kavenegar2fa_cancel']) ||
        $_GET['kavenegar2fa_cancel'] != 1) {
      return;
    }

    $this->session->unset('new_phone', 'login_verif');
  }

  public function on_show_own_profile ($user)
  {
    $this->forget_new_phone_if_currently_exists();
    $this->cancel_request_if_needed();

    $user_id = !empty($user->ID) ? $user->ID : get_current_user_id();
    $user_id or wp_die(__('Invalid user ID', 'kavenegar2fa'));

    $view = (object) [];

    $view->twofa_required = (bool) get_user_meta(
      $user_id,
      'kavenegar2fa_2fa_required',
      true
    );

    $has_phone = $this->session->has('new_phone');
    $has_verif = $this->session->has('profile_verif');
    $view->verif_disabled = !$has_phone || !$has_verif;

    if ($view->verif_disabled) {
      $view->phone = get_user_meta($user_id, 'kavenegar2fa_phone', true);
    } else {
      $view->phone = $this->session->get('new_phone');

      $qs = '?kavenegar2fa_cancel=1';
      $id = '#kavenegar2fa_phone-wrap';
      $view->cancel_url = admin_url("profile.php{$qs}{$id}");
    }

    $view->phone = $this->utils->format_phone_number($view->phone);

    $this->utils->render('own-profile-fields', $view);
  }

  public function on_show_user_profile ($user)
  {
    if (empty($user->ID)) {
      wp_die(__('Invalid user ID', 'kavenegar2fa'));
    }
    $user_id = $user->ID;

    $view = (object) [];

    $tfar_key = 'kavenegar2fa_2fa_required';
    $twofa_required = (bool) get_user_meta($user_id, $tfar_key, true);
    $view->twofa_required = $twofa_required;

    $phone_key = 'kavenegar2fa_phone';
    $phone = get_user_meta($user_id, $phone_key, true);
    $phone = $this->utils->format_phone_number($phone);
    $view->phone = $phone;

    $this->utils->render('user-profile-fields', $view);
  }

  public function filter_update_errors ($errors, $update, $user)
  {
    if (!$update) {
      return $this->filter_create_errors($errors, $update, $user);
    }

    if (empty($user->ID)) {
      wp_die(__('User ID (or whole user) is empty', 'kavenegar2fa'));
    }
    $self_edit = $this->is_self_edit($user);

    $phone = $this->utils->sanitize_request_phone();

    if (!$phone->empty && !$phone->value) {
      $msg = __('<strong>Error</strong>: Invalid phone number', 'kavenegar2fa');
      return $this->utils->error($errors, 'invalid_phone', $msg);
    }

    if ($phone->empty && $this->utils->get_option('phone_required')) {
      $msg = __('<strong>Error</strong>: Phone number is required', 'kavenegar2fa');
      return $this->utils->error($errors, 'empty_phone', $msg);
    }

    $found_user_id = $this->utils->find_user_id_by_phone($phone->value);
    if ($found_user_id && $found_user_id != $user->ID) {
      $msg = __('<strong>Error</strong>: This phone number is already exists, use another one', 'kavenegar2fa');
      return $this->utils->error($errors, 'duplicate_phone', $msg);
    }

    if (!$self_edit) {
      return $errors;
    }

    $verif = $this->utils->sanitize_request_verif();
    if (!$verif->empty && !$verif->value) {
      $msg = __('<strong>Error</strong>: Invalid verification code', 'kavenegar2fa');
      return $this->utils->error($errors, 'invalid_verif', $msg);
    }

    if ($phone->empty || $verif->empty) {
      return $errors;
    }

    $new_phone = $this->session->get('new_phone');

    if ($new_phone && $phone->value !== $new_phone) {
      $msg = __('<strong>Error</strong>: Phone number mismatch; If you want to change the phone number, leave verification code empty to receive a new code', 'kavenegar2fa');
      return $this->utils->error($errors, 'phone_mismatch', $msg);
    }
  }

  private function filter_create_errors ($errors, $ignore, $user)
  {
    $phone = $this->utils->sanitize_request_phone();

    if (!$phone->empty && !$phone->value) {
      $msg = __('<strong>Error</strong>: Invalid phone number', 'kavenegar2fa');
      return $this->utils->error($errors, 'invalid_phone', $msg);
    }

    $phone_required = (bool) $this->utils->get_option('phone_required', false);

    if ($phone_required && $phone->empty) {
      $msg = __('<strong>Error</strong>: Phone number is required', 'kavenegar2fa');
      return $this->utils->error($errors, 'empty_phone', $msg);
    }

    $uid = $this->utils->find_user_id_by_phone($phone->value);
    if ($uid && (empty($user->ID) || $uid != $user->ID)) {
      $msg = __('<strong>Error</strong>: This phone number is already exists, use another one', 'kavenegar2fa');
      return $this->utils->error($errors, 'duplicate_phone', $msg);
    }
  }

  private function update_2fa_required ($user_id)
  {
    $param = 'kavenegar2fa_2fa_required';
    $twofa_required = $this->utils->sanitize_request_bool($param);

    // simply, update 2fa_required meta according to twofa_required
    if ($twofa_required->value) {
      update_user_meta($user_id, 'kavenegar2fa_2fa_required', true);
    } else {
      delete_user_meta($user_id, 'kavenegar2fa_2fa_required');
    }
  }

  public function on_update_own_profile ($user_id)
  {
    empty($user_id) and wp_die('User ID is falsey');

    $this->update_2fa_required($user_id);

    $phone = $this->utils->sanitize_request_phone();

    // clear session and phone meta, if phone is absent
    if ($phone->empty) {
      $this->session->unset('new_phone');
      $this->session->unset('profile_verif');
      delete_user_meta($user_id, 'kavenegar2fa_phone');
      return; // verif (and its emptiness) doesnt matters
    } elseif (!$phone->value) {
      return;
    }

    $uid = $this->utils->find_user_id_by_phone($phone->value);
    if ($uid && $uid != $user_id) {
      return;
    }

    // do nothing, if phone is same as existing user meta value:
    $current_phone = get_user_meta($user_id, 'kavenegar2fa_phone', true);
    if ($phone->value === $current_phone) {
      $this->session->unset('new_phone', 'profile_verif');
      return;
    }

    $verif = $this->utils->sanitize_request_verif();

    // when verif is absent, it means the user wants to change his/her phone.
    // so, send a new verif code and store values to the session...
    if ($verif->empty) {
      // do nothing, if verification code has been sent
      $session_phone = $this->session->get('new_phone');
      if ($session_phone === $phone->value) {
        return;
      }

      $new_verif = $this->utils->generate_verif_code();
      $sms_error = $this->api->send_verif_code($phone->value, $new_verif);

      // if SMS not send, ask the user to try again
      if (is_wp_error($sms_error)) {
        $this->session->unset('new_phone');
        $this->session->unset('profile_verif');
        return;
      }

      $this->session->set('new_phone', $phone->value);
      $this->session->set('profile_verif', $new_verif);

      return;
    } elseif (!$verif->value) {
      return;
    }

    // do nothing, if phone and/or verif is not same as session values
    $session_phone = $this->session->get('new_phone');
    $session_verif = $this->session->get('profile_verif');
    if ($phone->value !== $session_phone ||
        $verif->value !== $session_verif) {
      return;
    }

    // update phone meta and clear session
    update_user_meta($user_id, 'kavenegar2fa_phone', $phone->value);
    $this->session->unset('new_phone');
    $this->session->unset('profile_verif');
  }

  public function on_update_user_profile ($user_id)
  {
    if (empty($user_id)) {
      wp_die(__('User ID is empty', 'kavenegar2fa'));
    }

    $this->update_2fa_required($user_id);

    $phone = $this->utils->sanitize_request_phone();

    $uid = $this->utils->find_user_id_by_phone($phone->value);
    if ($uid && $uid != $user_id) {
      return;
    }

    if ($phone->empty) {
      delete_user_meta($user_id, 'kavenegar2fa_phone');
    } elseif ($phone->value) {
      update_user_meta($user_id, 'kavenegar2fa_phone', $phone->value);
    }
  }

  public function on_user_new_form ($type)
  {
    if ($type !== 'add-new-user') {
      return;
    }

    $view = (object) [];

    $phone_required = $this->utils->get_option('phone_required');
    $view->phone_required = (bool) $phone_required;

    if (!empty($_REQUEST['kavenegar2fa_2fa_required'])) {
      $view->twofa_required = $_REQUEST['kavenegar2fa_2fa_required'];
    }

    if (!empty($_REQUEST['kavenegar2fa_phone'])) {
      $raw_phone = $_REQUEST['kavenegar2fa_phone'];
      $phone = $this->utils->sanitize_phone_number($raw_phone);
      $phone and $phone = $this->utils->format_phone_number($phone);

      $view->phone = $phone ? $phone : $raw_phone;
    }

    $this->utils->render('new-user-fields', $view);
  }

  public function on_user_register ($user_id)
  {
    if (!is_user_logged_in() ||
        get_current_user_id() == $user_id ||
        !current_user_can('create_users')) {
      return;
    }

    $tfar_param = 'kavenegar2fa_2fa_required';
    $twofa_required = $this->utils->sanitize_request_bool($tfar_param);

    if (!$twofa_required->empty && !$twofa_required->value) {
      wp_delete_user($user_id);
      return;
    }

    if ($twofa_required->value) {
      $tfar_meta = 'kavenegar2fa_2fa_required';
      $updated = update_user_meta($user_id, $tfar_meta, $twofa_required->value);

      if (!$updated) {
        wp_delete_user($user_id);
        return;
      }
    }

    $phone = $this->utils->sanitize_request_phone();

    if (!$phone->empty && !$phone->value) {
      wp_delete_user($user_id);
      return;
    }

    if ($phone->value) {
      $phone_meta = 'kavenegar2fa_phone';
      $updated = update_user_meta($user_id, $phone_meta, $phone->value);

      if (!$updated) {
        wp_delete_user($user_id);
        return;
      }
    }
  }

  private function is_self_edit ($user)
  {
    if (defined('IS_PROFILE_PAGE')) {
      return (bool) IS_PROFILE_PAGE;
    }

    if (!empty($user->ID)) {
      return $user->ID == get_current_user_id();
    }

    wp_die(__('Cannot determine update type', 'kavenegar2fa'));
  }
}
