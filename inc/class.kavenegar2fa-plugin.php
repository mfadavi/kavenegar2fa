<?php

defined('ABSPATH') or die();

class Kavenegar2FA_Plugin
{
  private $main;
  private $admin;
  private $login;
  private $rest;
  private $profile;
  private $utils;

  public function __construct ($scope)
  {
    $this->main = $scope->main;
    $this->admin = $scope->admin;
    $this->login = $scope->login;
    $this->rest = $scope->rest;
    $this->profile = $scope->profile;
    $this->utils = $scope->utils;
  }

  public function initialize ()
  {
    register_activation_hook($this->main, [$this, 'on_activation']);
    register_deactivation_hook($this->main, [$this, 'on_deactivation']);

    add_action('plugins_loaded', [$this, 'on_plugins_loaded']);

    if (function_exists('is_multisite') && is_multisite())
    {
      add_action('admin_notices', [$this, 'on_admin_notices']);
    }

    $this->admin->initialize();
    $this->login->initialize();
    $this->rest->initialize();
    $this->profile->initialize();
  }

  public function on_admin_notices ()
  {
    $content = __('Kavenegar 2FA is not tested in multisite mode and its functionality is not guaranteed.', 'kavenegar2fa');
    $this->utils->admin_notice([
      'content' => $content
    ]);
  }

  public function on_plugins_loaded ()
  {
    $base_dir = basename(dirname($this->main));
    load_plugin_textdomain('kavenegar2fa', false, "$base_dir/languages/");
  }

  public function on_activation ()
  {
    add_option('kavenegar2fa', [
      'api_key' => '',
      'phone_required' => false,
    ]);
  }

  public function on_deactivation ()
  {
  }

}
