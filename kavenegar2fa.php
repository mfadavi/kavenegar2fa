<?php
/*
  Plugin Name: Kavenegar 2FA
  Plugin URI: https://gitlab.com/fadavi.net/kavenegar2fa
  Description: Two-Factor Authentication (2FA) via Kavenegar SMS Service
  Version: 1.0.0
  Author: Mohamad Fadavi
  Author URI: https://fadavi.net
  License: GPLv2 or later
  Text Domain: kavenegar2fa
  Domain Path: /languages
*/

defined('ABSPATH') or die();

function_exists('kavenegar2fa_setup') and die('Dup. fn: kavenegar2fa_setup');
require_once plugin_dir_path(__FILE__) . 'inc/function.kavenegar2fa-setup.php';

kavenegar2fa_setup((object) [
  'version' => '1.0.0',

  'basename' => plugin_basename(__FILE__),
  'root' => plugin_dir_path(__FILE__),
  'main' => __FILE__,
  'debug' => false,
]);

return;

/* these dead lines will help MOEdit to index translations: */
__('Kavenegar 2FA', 'kavenegar2fa');
__('Two-Factor Authentication (2FA) via Kavenegar SMS Service', 'kavenegar2fa');
__('Mohamad Fadavi', 'kavenegar2fa');
